package com.showe.service;

import com.showe.config.FeignConfiguration;
import com.showe.model.User;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Author: Xiaowei Ni
 * @Description:
 * @Date: Created in 22:24 2019/3/23
 * @Modified By:
 */
@FeignClient(name="microservice-provider-user",configuration = FeignConfiguration.class)
public interface UserFeignClient {

    @RequestLine(value = "GET /{id}")
    public User findById(@Param("id") Long id);
}

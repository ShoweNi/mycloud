package com.showe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: Xiaowei Ni
 * @Description:
 * @Date: Created in 17:11 2019/3/24
 * @Modified By:
 */
@SpringBootApplication
public class ConifgClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConifgClientApplication.class,args);
    }
}

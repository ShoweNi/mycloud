package com.showe.controller;


import com.showe.dao.UserRepository;
import com.showe.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Xiaowei Ni
 * @Description:
 * @Date: Created in 22:36 2019/3/20
 * @Modified By:
 */
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/{id}")
    public User findById(@PathVariable("id") Long id){
        User user = this.userRepository.findById(id).orElse(new User());

        return user;
    }
}

package com.showe.dao;


import com.showe.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: Xiaowei Ni
 * @Description:
 * @Date: Created in 22:35 2019/3/20
 * @Modified By:
 */
public interface UserRepository extends JpaRepository<User,Long> {
}

package com.showe.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @Author: Xiaowei Ni
 * @Description:
 * @Date: Created in 14:28 2019/3/24
 * @Modified By:
 */
@Controller
public class FileUploadController {

    /**
     * 上传文件
     * 测试方法：
     * 使用命令 curl -F "file=@文件全名" localhost:8050/upload
     * @param file
     * @return 文件在服务骑上的绝对路径
     * @throws IOException
     */
    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    @ResponseBody
    public String handleFileUpload(@RequestParam(value="file",required = true)MultipartFile file) throws IOException{
        byte[] bytes = file.getBytes();
       File file1ToSave = new File(file.getOriginalFilename());
        FileCopyUtils.copy(bytes,file1ToSave);
        return file1ToSave.getAbsolutePath();
    }
}

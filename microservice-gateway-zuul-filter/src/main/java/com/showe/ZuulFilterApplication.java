package com.showe;

import com.showe.filter.PreRequestLogFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 * @Author: Xiaowei Ni
 * @Description:
 * @Date: Created in 14:51 2019/3/24
 * @Modified By:
 */
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication
public class ZuulFilterApplication {

    @Bean
    public PreRequestLogFilter preRequestLogFilter(){
        return new PreRequestLogFilter();
    }

    public static void main(String[] args) {
        SpringApplication.run(ZuulFilterApplication.class,args);
    }
}
